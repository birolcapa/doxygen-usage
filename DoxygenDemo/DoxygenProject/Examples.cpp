#include "Examples.h"
#include "Src/Shape.h"
#include "Src/Circle.h"
#include "Src/Rectangle.h"

Examples::Examples(void)
{
}


Examples::~Examples(void)
{
}

void Examples::TestCircle()
{
	//![TestCircle]
	Circle testCircle(15, 25, 8);
	testCircle.GetRadius();
	testCircle.SetRadius(58);
	testCircle.Draw();
	//![TestCircle]
}

void Examples::TestRectangle()
{
	//![TestRectangle]
	Rectangle testRectangle(8,7,15,26);
	testRectangle.GetHeight();
	testRectangle.SetHeight(16);
	testRectangle.GetWidth();
	testRectangle.SetWidth(47);
	testRectangle.Draw();
	//![TestRectangle]
}

