#include "Shape.h"
#include "Circle.h"
#include <iostream>

Circle::Circle(int newx, int newy, int newradius): Shape(newx, newy) 
{
   SetRadius(newradius);
}

int Circle::GetRadius()
{
	return m_Radius;
}

void Circle::SetRadius(int newradius)
{
	m_Radius = newradius;
}

void Circle::Draw() 
{
	std::cout << "Drawing a Circle at:(" <<
		GetX() << "," << GetY() << "), radius "
		<< GetRadius() << std::endl;
}