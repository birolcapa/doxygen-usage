#include "Shape.h"
#include "Rectangle.h"
#include <iostream>

Rectangle::Rectangle(int newx, int newy, int newwidth, int newheight): Shape(newx, newy) 
{
   SetWidth(newwidth);
   SetHeight(newheight);
}

int Rectangle::GetWidth()
{
	return m_Width;
}

int Rectangle::GetHeight()
{
	return m_Height;
}

void Rectangle::SetWidth(int newwidth)
{
	m_Width = newwidth;
}

void Rectangle::SetHeight(int newheight)
{
	m_Height = newheight;
}

void Rectangle::Draw() {
	std::cout << "Drawing a Rectangle at:(" <<
		GetX() << "," << GetY() << "), width " << GetWidth() <<
		", height " << GetHeight() << std::endl;
}