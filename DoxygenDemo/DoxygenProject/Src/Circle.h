#pragma once
#include "Shape.h"

/** \defgroup groupCircleClass Circle Class
 * \brief Circle class is used to create a circle.
 *
 * With this class, following functions can be done:
 * - Set Radius
 * - Get Radius
 * - Draw Circle
 * @{
 */
class Circle: public Shape 
{
public:
  /**
   * Circle Constructor.
   * @param newx x value of the center.
   * @param newy y value of the center.
   * @param newradius radius of the circle.
   */
   Circle(int newx, int newy, int newradius);

  /**
   * Gets radius.
   */
   int GetRadius();

  /**
   * Sets radius Constructor.
   * @param newradius radius of the circle.
   */
   void SetRadius(int newradius);

  /**
   * Draws the circle.
   */
   void Draw();
private:

   /**
	* Radius of the circle.
	*/
	int m_Radius;
};

/** @} */
