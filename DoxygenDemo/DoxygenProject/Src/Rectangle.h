#pragma once
#include "Shape.h"

/** \defgroup groupRectangleClass Rectangle Class
 * \brief Rectangle class is used to create a rectangle.
 *
 * With this class, following functions can be done:
 * - Set width
 * - Get width
 * - Set height
 * - Get height
 * - Draw Rectangle
 * @{
 */
class Rectangle: public Shape 
{
public:
    /// <summary>
	/// Constructor of Rectangle class
	/// </summary>
	/// <param name="newx">x value</param>
	/// <param name="newy">y value</param>
	/// <param name="newwidth">Width of the rectangle</param>
	/// <param name="newheight">Height of the rectangle</param>
	Rectangle(int newx, int newy, int newwidth, int newheight);

	/// <summary>
	/// Gets width.
	/// </summary>
	int GetWidth();

    /// <summary>
	/// Gets height.
	/// </summary>
	int GetHeight();

	/// <summary>
	/// Sets width.
	/// </summary>
	/// <param name="newwidth">Width value</param>
	void SetWidth(int newwidth);

	/// <summary>
	/// Sets height.
	/// </summary>
	/// <param name="newheight">Height value</param>
	void SetHeight(int newheight);

	/// <summary>
	/// Draws rectangle to console.
	/// </summary>
	void Draw();
private:

	/// <summary>
	/// Width of the rectangle
	/// </summary>
	int m_Width;

	/// <summary>
	/// Height of the rectangle
	/// </summary>
	int m_Height;
};
/** @} */
