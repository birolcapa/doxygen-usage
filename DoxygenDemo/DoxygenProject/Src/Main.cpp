#include <iostream>
#include "Shape.h"
#include "Circle.h"
#include "Rectangle.h"

/*! \mainpage Drawing Shapes
 *
 * This project helps user to draw shapes.
 * Currently two types of shapes can be drawn:
 * - \subpage drawingRectanglePage "How to draw rectangle?"
 *
 * - \subpage drawingCirclePage "How to draw circle?"
 *
 */

/*! \page drawingRectanglePage How to draw rectangle?
 *
 * This page is about how to draw a rectangle.
 * Following sections describe rectangle:
 * - \ref groupRectangleDefinition "Definition of Rectangle"
 * - \ref groupRectangleClass "Rectangle Class"
 * - \ref groupRectangleUsage "How to use rectangle class"
 *
 */

/** \defgroup groupRectangleUsage How to use rectangle class
 * Rectangle class can be used as following:
 * \snippet Examples.cpp TestRectangle
 */

/** \defgroup groupRectangleDefinition Rectangle Definition
 * In Euclidean plane geometry, a rectangle is any quadrilateral with four right angles.
 * It can also be defined as an equiangular quadrilateral,
 * since equiangular means that all of its angles are equal
 */

/*! \page drawingCirclePage How to draw circle?
 *
 * This page is about how to draw a circle.
 * Following sections describe circle:
 * - \ref groupCircleDefinition "Definition of Circle"
 * - \ref groupCircleClass "Circle Class"
 * - \ref groupCircleUsage "How to use circle class"
 */

/** \defgroup groupCircleDefinition Circle Definition
 * A circle is a simple shape in Euclidean geometry.
 * It is the set of all points in a plane that are at a given distance from a given point, the centre;
 * equivalently it is the curve traced out by a point that moves so that its distance from a given point is constant.
 * The distance between any of the points and the centre is called the radius.
 */

/** \defgroup groupCircleUsage Using circle class
 * Circle class can be used as following:
 * \snippet Examples.cpp TestCircle
 */

int main(void) 
{
   // set up array to the shapes
   Shape *shapes[2];
   shapes[0] = new Rectangle(10, 20, 5, 6);
   shapes[1] = new Circle(15, 25, 8);

   // iterate through the array and handle shapes polymorphically
   for (int i = 0; i < 2; i++) 
   {
      shapes[i]->Draw();
      shapes[i]->RMoveTo(100, 100);
      shapes[i]->Draw();
   }

   // call a rectangle specific function
   Rectangle *arec = new Rectangle(0, 0, 15, 15);
   arec->SetWidth(30);
   arec->Draw();
   system("pause");
}