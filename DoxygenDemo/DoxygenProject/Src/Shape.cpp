#include "Shape.h"

Shape::Shape(int newx, int newy) 
{
   MoveTo(newx, newy);
}

int Shape::GetX()
{
	return m_X;
}

int Shape::GetY()
{
	return m_Y;
}

void Shape::SetX(int newx)
{
	m_X = newx;
}

void Shape::SetY(int newy)
{
	m_Y = newy;
}

void Shape::MoveTo(int newx, int newy) 
{
   SetX(newx);
   SetY(newy);
}

void Shape::RMoveTo(int deltax, int deltay) 
{
   MoveTo(GetX() + deltax, GetY() + deltay);
}

void Shape::Draw() 
{

}
