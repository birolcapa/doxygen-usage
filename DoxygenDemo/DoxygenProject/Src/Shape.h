#pragma once
class Shape 
{
public:
	/// <summary>
	/// Constructor of Shape class.
	/// Moves the shape position.
	/// </summary>
	/// <param name="newx">x value</param>
	/// <param name="newy">y value</param>
	Shape(int newx, int newy);

	virtual ~Shape(){};

	/// <summary>
	/// Gets x value.
	/// </summary>
	int GetX();

	/// <summary>
	/// Gets y value.
	/// </summary>
	int GetY();

	/// <summary>
	/// Sets x value.
	/// </summary>
	/// <param name="newx">Desired x value</param>
	void SetX(int newx);

	/// <summary>
	/// Sets y value.
	/// </summary>
	/// <param name="newy">Desired y value</param>
	void SetY(int newy);

	/// <summary>
	/// Constructor of Shape class.
	/// Moves the shape position
	/// </summary>
	/// <param name="newx">x value</param>
	/// <param name="newy">y value</param>
	void MoveTo(int newx, int newy);

	/// <summary>
	/// Moves the shape position with respect to delta x and delta y
	/// </summary>
	/// <param name="deltax">delta x value</param>
	/// <param name="deltay">delta y value</param>
	void RMoveTo(int deltax, int deltay);

	/// <summary>
	/// Abstract draw method.
	/// </summary>
	virtual void Draw();
private:
	/// <summary>
	/// x position
	/// </summary>
	int m_X;

	/// <summary>
	/// y position
	/// </summary>
	int m_Y;
};
